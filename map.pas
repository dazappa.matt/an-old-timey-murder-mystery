unit map;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

const
  LAYER_GROUND = 0;
  LAYER_MASK = 1;
  LAYER_MASK2 = 2;
  LAYER_FRINGE = 3;
  LAYER_FRINGE2 = 4;
  BLOCKED = 1025;
  PLAYER_START = 1026;
  WARP = 1027;

type
  TMapObject = record
    objType: Integer;
    x, y: Integer;
    data1, data2, data3: Integer;
  end;

  TMap = record
    groundEmpty, maskEmpty, mask2Empty, fringeEmpty, fringe2Empty: boolean;
    numRows, numCols: Integer;
    ground, mask, mask2, fringe, fringe2, collision: array of array of Integer;
    objects: array of TMapObject;
  end;

implementation

end.

