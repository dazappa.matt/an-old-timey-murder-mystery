unit assets;

{$mode objfpc}{$H+}

interface

uses
  {$ifdef unix}cthreads,{$endif}Classes, SysUtils, SDL2, SDL2_Image, maploader, map;

procedure InitAssets(var r: PSDL_Renderer);
procedure CleanUpAssets();

const
  TILEMAP_SIZE = 32; // How many tiles across fit on each side of the square tile map. Tiles are 16x16px

var
  spriteFont, tilemap, menubg, selectArrow: PSDL_Texture;
  pdb: PSDL_Texture;
  mTown: TMap;

implementation

procedure InitAssets(var r: PSDL_Renderer);
begin
  writeln('font');
  spriteFont := IMG_LoadTexture(r, 'assets/textures/font.png');
  if spriteFont = nil then HALT;

  writeln('menu bg');
  menubg := IMG_LoadTexture(r, 'assets/textures/menu_bg.png');
  if menubg = nil then HALT;

  writeln('tile map');
  tilemap := IMG_LoadTexture(r, 'assets/textures/tiles.png');
  if tilemap = nil then HALT;

  writeln('select arrow');
  selectArrow := IMG_LoadTexture(r, 'assets/textures/select_arrow.png');
  if selectArrow = nil then HALT;

  pdb := IMG_LoadTexture(r, 'assets/textures/player_debug.png');
  if pdb = nil then HALT;

  mTown := LoadMap('assets/maps/town.tmx');
end;

procedure CleanUpAssets();
begin
  SDL_DestroyTexture(menubg);
  SDL_DestroyTexture(spriteFont);
  SDL_DestroyTexture(tilemap);
  SDL_DestroyTexture(pdb);
end;

end.

