# An Old Timey Murder Mystery
A murder mystery game for GameOff 2017.

# Controls
1. ESC - Quit
1. F11 - Fullscreen

# Compiling
1. Download the Lazarus IDE from http://www.lazarus-ide.org/
1. Download and place the Pascal SDL2 units in your path: https://github.com/ev1313/Pascal-SDL-2-Headers
1. Download the compiled release version shared libraries for SDL2, and SDL2_Image. In Windows, place them in same directory as the executable.