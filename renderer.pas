unit renderer;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, SDL2, assets, gamestate;

const
  FONT_ORDER: array[0..62] of string = ('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
    'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '!', '@', '#',
    '$', '%', '^', '&', '*', '(', ')', ':', ';', '''', '"', ',', '.', '<', '>', '?', '/', '-', '+', '=', '[', ']', '~', '|');

function InitRenderer(var w: PSDL_Window): PSDL_Renderer;
procedure Render();
procedure DrawText(t: string; x, y: integer);
procedure DrawMapBelow();
procedure DrawMapAbove();
procedure CleanUpRenderer();

function GetFullScreen(): boolean;

procedure SetWindowSize(w, h: Integer);
procedure SetCanvasScale(cs: Integer);
procedure SetFullScreen(newFS: boolean);

implementation
var
  windowWidth, windowHeight: Integer;
  canvasWidth, canvasHeight: Integer;
  canvasScaleFactor: Integer;
  fullScreen: boolean;
  r: PSDL_Renderer;
  c: PSDL_Texture; // Canvas/Buffer. Draw to here and scale to window dimensions.
  window: PSDL_Window;
  srcR, destR: PSDL_Rect; // Blitting rectangle
  tileDrawCount: Integer; // DEBUG

// TODO: change to TSDL_Renderer...?
function InitRenderer(var w: PSDL_Window): PSDL_Renderer;
begin
  // Setup renderer - TODO: load from a config file
  windowWidth := 1280;
  windowHeight := 720;
  canvasScaleFactor  := 4; // 1, 2, 4, 8, 16 -- render resolution will be window/renderScaling
  canvasWidth := windowWidth div canvasScaleFactor;
  canvasHeight := windowHeight div canvasScaleFactor;
  fullScreen := false;

  window := w;
  SetWindowSize(windowWidth, windowHeight);

  writeln('Create renderer');
  r := SDL_CreateRenderer(window, -1, 0);
  if r = nil then
  begin
    writeln('Unable to create renderer');
    HALT;
  end;

  c := SDL_CreateTexture(r, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, canvasWidth, canvasHeight);
  new(destR);
  new(srcR);

  Result := r;
end;

procedure CleanUpRenderer();
begin
  SDL_DestroyTexture(c);
  dispose(destR);
  dispose(srcR);
  SDL_DestroyRenderer(r);
end;

procedure SetScreen(newScreen: Integer);
begin
  screen := newScreen;
end;

procedure ResetCanvas();
begin
  SDL_DestroyTexture(c);
  canvasWidth := windowWidth div canvasScaleFactor;
  canvasHeight := windowHeight div canvasScaleFactor;
  c := SDL_CreateTexture(r, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, canvasWidth, canvasHeight);
end;

procedure SetWindowSize(w, h: Integer);
begin
  windowWidth := w;
  windowHeight := h;
  SDL_SetWindowSize(window, w, h);
  SDL_SetWindowPosition(window, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
  ResetCanvas();
end;

procedure SetCanvasScale(cs: Integer);
begin
  canvasScaleFactor := cs;
  ResetCanvas();
end;

function GetFullScreen(): boolean;
begin
  Result := fullScreen;
end;

procedure SetFullScreen(newFS: boolean);
begin
  fullScreen := newFS;
  if fullScreen then
  begin
    SDL_SetWindowFullScreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP);
  end else
  begin
    SDL_SetWindowFullScreen(window, 0);
  end;
end;

procedure DrawText(t: string; x, y: integer);
var
  i, j: integer; // Counter variables
  startX, startY: integer; // Pixel pos of character to copy to screen
begin
  i := 1;
  j := 0;
  startX := 0;
  startY := 0;
  t := LowerCase(t);

  // For each character in the text we want to draw
  while i <= Length(t) do
  begin
    // Search for character index in font string array
    j := 0;
    while j < 63 do
    begin
      if t[i] = FONT_ORDER[j] then
      begin
        startY := 0;
        // Calculate starting coordinates of character in font sheet
        while j > 7 do
        begin
          startY := startY + 8;
          j := j - 8;
        end;
        startX := j*8;

        // Set up blitting rectangles
        srcR^.x := startX;
        srcR^.y := startY;
        srcR^.w := 8;
        srcR^.h := 8;
        destR^.x := x + 8*i - 8; // -8 because i is 1-based
        destR^.y := y;
        destR^.w := 8;
        destR^.h := 8;

        SDL_RenderCopy(r, spriteFont, srcR, destR);

        j := 63; // Break loop, go to next char in string
      end;

      Inc(j);
    end;

    Inc(i);
  end;
end;

// TODO: pass var tmap to this function
procedure DrawMapBelow();
var
  tileIndex, startX, startY, camTileX, camTileY, x, y: integer;
begin
  tileDrawCount := 0;
  y := 0;
  camTileY := cam^.y div 16;
  while y < camHT+1 do
  begin
    x := 0;
    camTileX := cam^.x div 16;
    while x < camWT+1 do
    begin
      if (camTileX > -1) and (camTileY > -1) then
      begin
        tileIndex := mTown.ground[camTileY, camTileX];
        if tileIndex > 0 then
        begin
          Inc(tileDrawCount);
          // Determine what coordinates from the tilemap we need to blit from
          startY := 0;
          while tileIndex > TILEMAP_SIZE do
          begin
            startY := startY + 16;
            tileIndex := tileIndex - TILEMAP_SIZE;
          end;
          startX := tileIndex*16 - 16; // Tile indexes start at 1. Zero is blank.

          // Set up blitting rectangles
          srcR^.x := startX;
          srcR^.y := startY;
          srcR^.w := 16;
          srcR^.h := 16;
          destR^.x := camTileX*16 - cam^.x;
          destR^.y := camTileY*16 - cam^.y;
          destR^.w := 16;
          destR^.h := 16;

          SDL_RenderCopy(r, tilemap, srcR, destR);
        end;

        tileIndex := mTown.mask[camTileY, camTileX];
        if tileIndex > 0 then
        begin
          Inc(tileDrawCount);
          // Determine what coordinates from the tilemap we need to blit from
          startY := 0;
          while tileIndex > TILEMAP_SIZE do
          begin
            startY := startY + 16;
            tileIndex := tileIndex - TILEMAP_SIZE;
          end;
          startX := tileIndex*16 - 16; // Tile indexes start at 1. Zero is blank.

          // Set up blitting rectangles
          srcR^.x := startX;
          srcR^.y := startY;
          srcR^.w := 16;
          srcR^.h := 16;
          destR^.x := camTileX*16 - cam^.x;
          destR^.y := camTileY*16 - cam^.y;
          destR^.w := 16;
          destR^.h := 16;

          SDL_RenderCopy(r, tilemap, srcR, destR);
        end;

        tileIndex := mTown.mask2[camTileY, camTileX];
        if tileIndex > 0 then
        begin
          Inc(tileDrawCount);
          // Determine what coordinates from the tilemap we need to blit from
          startY := 0;
          while tileIndex > TILEMAP_SIZE do
          begin
            startY := startY + 16;
            tileIndex := tileIndex - TILEMAP_SIZE;
          end;
          startX := tileIndex*16 - 16; // Tile indexes start at 1. Zero is blank.

          // Set up blitting rectangles
          srcR^.x := startX;
          srcR^.y := startY;
          srcR^.w := 16;
          srcR^.h := 16;
          destR^.x := camTileX*16 - cam^.x;
          destR^.y := camTileY*16 - cam^.y;
          destR^.w := 16;
          destR^.h := 16;

          SDL_RenderCopy(r, tilemap, srcR, destR);
        end;
      end;
      Inc(camTileX);
      Inc(x);
    end;
    Inc(camTileY);
    Inc(y);
  end;
end;

procedure DrawMapAbove();
var
  tileIndex, startX, startY, camTileX, camTileY, x, y: integer;
begin
  y := 0;
  camTileY := cam^.y div 16;
  while y < camHT+1 do
  begin
    x := 0;
    camTileX := cam^.x div 16;
    while x < camWT+1 do
    begin
      if (camTileX > -1) and (camTileY > -1) then
      begin
        tileIndex := mTown.fringe[camTileY, camTileX];
        if tileIndex > 0 then
        begin
          Inc(tileDrawCount);
          // Determine what coordinates from the tilemap we need to blit from
          startY := 0;
          while tileIndex > TILEMAP_SIZE do
          begin
            startY := startY + 16;
            tileIndex := tileIndex - TILEMAP_SIZE;
          end;
          startX := tileIndex*16 - 16; // Tile indexes start at 1. Zero is blank.

          // Set up blitting rectangles
          srcR^.x := startX;
          srcR^.y := startY;
          srcR^.w := 16;
          srcR^.h := 16;
          destR^.x := camTileX*16 - cam^.x;
          destR^.y := camTileY*16 - cam^.y;
          destR^.w := 16;
          destR^.h := 16;

          SDL_RenderCopy(r, tilemap, srcR, destR);
        end;

        tileIndex := mTown.fringe2[camTileY, camTileX];
        if tileIndex > 0 then
        begin
          Inc(tileDrawCount);
          // Determine what coordinates from the tilemap we need to blit from
          startY := 0;
          while tileIndex > TILEMAP_SIZE do
          begin
            startY := startY + 16;
            tileIndex := tileIndex - TILEMAP_SIZE;
          end;
          startX := tileIndex*16 - 16; // Tile indexes start at 1. Zero is blank.

          // Set up blitting rectangles
          srcR^.x := startX;
          srcR^.y := startY;
          srcR^.w := 16;
          srcR^.h := 16;
          destR^.x := camTileX*16 - cam^.x;
          destR^.y := camTileY*16 - cam^.y;
          destR^.w := 16;
          destR^.h := 16;

          SDL_RenderCopy(r, tilemap, srcR, destR);
        end;
      end;
      Inc(camTileX);
      Inc(x);
    end;
    Inc(camTileY);
    Inc(y);
  end;
end;

procedure Render();
begin
  case screen of
    SCREEN_GAME:
    begin
      SDL_SetRenderTarget(r, c);
      SDL_RenderClear(r);
      DrawMapBelow();

      // Draw Player
      destR^.x := Round(playerX - camX);
      destR^.y := Round(playerY - camY);
      destR^.w := 16;
      destR^.h := 16;
      SDL_RenderCopy(r, pdb, nil, destR);

      // Draw NPC
      destR^.x := Round(npcX - camX);
      destR^.y := Round(npcY - camY);
      destR^.w := 16;
      destR^.h := 16;
      SDL_RenderCopy(r, pdb, nil, destR);

      DrawMapAbove();

      DrawText('Tiles: '+IntToStr(tileDrawCount), 1, 2);
      //DrawText('Cam: '+IntToStr(Round(camX))+','+IntToStr(Round(camY)), 1, 2+9);
      DrawText('Player: '+IntToStr(Round(playerX))+','+IntToStr(Round(playerY)), 1, 2+9);

      if fadeIn or fadeOut then
      begin
        destR^.x := 0;
        destR^.y := 0;
        destR^.w := 320;
        destR^.h := 180;
        SDL_SetRenderDrawBlendMode(r, SDL_BLENDMODE_BLEND);
        SDL_SetRenderDrawColor(r, 0, 0, 0, Round(fadeAlpha));
        SDL_RenderFillRect(r, destR);
        SDL_SetRenderDrawBlendMode(r, SDL_BLENDMODE_NONE);
      end;

      SDL_SetRenderTarget(r, nil);
      SDL_RenderClear(r);
      SDL_RenderCopy(r, c, nil, nil);
      SDL_RenderPresent(r);
    end;
    SCREEN_MAIN_MENU:
    begin
      // Render to buffer texture
      SDL_SetRenderTarget(r, c);
      SDL_RenderClear(r);
      SDL_RenderCopy(r, menubg, nil, nil);
      DrawText('An Old Timey Murder Mystery', 320 div 2 - 216 div 2, 24);
      //DrawText('DT: '+IntToStr(Round(blinkCounter)), 2, 150);
      DrawText('Play', 264, 72);
      DrawText('Quit', 264, 72 + 12);
      case menuSelected of
        OPT_PLAY:
        begin
          destR^.x := 264-8;
          destR^.y := 72;
        end;
        OPT_QUIT:
        begin
          destR^.x := 264-8;
          destR^.y := 72+12;
        end;
      end;
      destR^.w := 8;
      destR^.h := 8;
      if blinkVisible then
        SDL_RenderCopy(r, selectArrow, nil, destR);

      if not(blinkEnabled) then
      begin
        destR^.x := 0;
        destR^.y := 0;
        destR^.w := 320;
        destR^.h := 180;
        SDL_SetRenderDrawBlendMode(r, SDL_BLENDMODE_BLEND);
        SDL_SetRenderDrawColor(r, 0, 0, 0, Round(fadeAlpha));
        SDL_RenderFillRect(r, destR);
        SDL_SetRenderDrawBlendMode(r, SDL_BLENDMODE_NONE);
      end;

      SDL_SetRenderTarget(r, nil);
      SDL_RenderClear(r);
      SDL_RenderCopy(r, c, nil, nil);
      SDL_RenderPresent(r);
    end;
    SCREEN_OPTIONS:
    begin

    end;
  end;
end;

end.

