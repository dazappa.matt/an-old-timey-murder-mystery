program game;

uses SDL2, SDL2_Image, maploader, assets, gridmovement, renderer, gameloop,
  config, eventhandler, map, gamestate;

var
  window: PSDL_Window;
  r: PSDL_Renderer;
  ev: PSDL_Event;

begin
  // Initialize SDL
  writeln('Hello, hope you''re having a nice day.');
  writeln('SDL init');
  if SDL_Init(SDL_INIT_EVERYTHING) < 0 then HALT;

  writeln('Create window');
  window := SDL_CreateWindow('An Old Timey Murder Mystery', SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 720, SDL_WINDOW_SHOWN);
  if window = nil then
  begin
    writeln('Unable to create window');
    HALT;
  end;

  SDL_ShowCursor(SDL_Disable);

  r := InitRenderer(window);

  InitGameState();

  // Load in game data
  writeln('Load data');
  InitAssets(r);

  InitInput();
  InitGameLoop();

  screen := SCREEN_MAIN_MENU;

  gameRunning := true;
  new(ev);

  while gameRunning do
  begin
    while SDL_PollEvent(ev) = 1 do
    begin
      case ev^.type_ of
        SDL_KEYDOWN: begin
          case ev^.key.keysym.sym of
            SDLK_ESCAPE: begin
              gameRunning := false;
            end;
          end;
        end;

        SDL_WINDOWEVENT: begin
          case ev^.window.event of
            SDL_WINDOWEVENT_CLOSE: begin
              writeln('Window close - shutdown');
              gameRunning := false;
            end;
            SDL_WINDOWEVENT_FOCUS_GAINED: begin
              //writeln('Window focus gained');
            end;
            SDL_WINDOWEVENT_FOCUS_LOST: begin
              //writeln('Window focus lost');
            end;
          end;
        end;
      end;

      HandleInput(ev);
    end;

    GameLogic();
    Render();

    SDL_Delay(1); // CPU saver
  end;

  writeln('Cleaning up and quitting game');
  writeln('Thanks for playing');
  CleanUpRenderer();
  CleanUpAssets();
  CleanUpGameState();
  dispose(ev);
  SDL_DestroyWindow(window);

  SDL_Quit;
end.

