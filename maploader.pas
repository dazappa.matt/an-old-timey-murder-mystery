unit maploader;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, map, gamestate;

function LoadMap(fname: string): TMap;

implementation

// NOTE: this is not robust. You better feed it the right files or it'll probably die spectacularly.
function LoadMap(fname: string): TMap;
var
  m: TMap;
  f: TextFile;
  t, tmpStr: string;
  tileList: TStrings;
  i, j, objID, objX, objY, tmpI: integer;
begin
  m.groundEmpty := true;
  m.maskEmpty := true;
  m.mask2Empty := true;
  m.fringeEmpty := true;
  m.fringe2Empty := true;

  AssignFile(f, fname);
  Reset(f);
  // Skip some lines
  ReadLn(f, t);
  ReadLn(f, t);
  ReadLn(f, t);

  while not Eof(f) do
  begin
    ReadLn(f, t);
    t := Trim(t);
    if AnsiPos('ground', t) > 0 then
    begin
      // Extract map dimensions
      tmpStr := Copy(t, AnsiPos('width', t)+7, Length(t));
      tmpStr := Copy(tmpStr, 1, AnsiPos('"', tmpStr)-1);
      m.numCols := StrToInt(tmpStr);
      tmpStr := Copy(t, AnsiPos('height', t)+8, Length(t));
      tmpStr := Copy(tmpStr, 1, AnsiPos('"', tmpStr)-1);
      m.numRows := StrToInt(tmpStr);

      WriteLn(fname+' '+IntToStr(m.numCols)+'x'+IntToStr(m.numRows));

      SetLength(m.ground, m.numRows);
      SetLength(m.mask, m.numRows);
      SetLength(m.mask2, m.numRows);
      SetLength(m.fringe, m.numRows);
      SetLength(m.fringe2, m.numRows);
      SetLength(m.collision, m.numRows);
      j := 0;
      while j < m.numRows do
      begin
        SetLength(m.ground[j], m.numCols);
        SetLength(m.mask[j], m.numCols);
        SetLength(m.mask2[j], m.numCols);
        SetLength(m.fringe[j], m.numCols);
        SetLength(m.fringe2[j], m.numCols);
        SetLength(m.collision[j], m.numCols);
        Inc(j);
      end;

      // Read in CSV layer data
      ReadLn(f, t);
      ReadLn(f, t);
      j := 0;
      while not(t = '</data>') do
      begin
        // Split string and populate array with tile numbers
        tileList := TStringList.Create;
        try
          ExtractStrings([','], [], PChar(t), tileList);
          i := 0;
          while i < tileList.Count do
          begin
            m.ground[j,i] := StrToInt(tileList[i]);
            if m.ground[j,i] > 0 then
            begin
              m.groundEmpty := false;
            end;
            Inc(i);
          end;
        finally
          tileList.Free;
        end;
        Inc(j);
        ReadLn(f, t);
      end;
    end;

    if AnsiPos('mask"', t) > 0 then
    begin
      // Read in CSV layer data
      ReadLn(f, t);
      ReadLn(f, t);
      j := 0;
      while not(t = '</data>') do
      begin
        // Split string and populate array with tile numbers
        tileList := TStringList.Create;
        try
          ExtractStrings([','], [], PChar(t), tileList);
          i := 0;
          while i < tileList.Count do
          begin
            m.mask[j,i] := StrToInt(tileList[i]);
            if m.mask[j,i] > 0 then
            begin
              m.maskEmpty := false;
            end;
            Inc(i);
          end;
        finally
          tileList.Free;
        end;
        Inc(j);
        ReadLn(f, t);
      end;
    end;

    if AnsiPos('mask2', t) > 0 then
    begin
      // Read in CSV layer data
      ReadLn(f, t);
      ReadLn(f, t);
      j := 0;
      while not(t = '</data>') do
      begin
        // Split string and populate array with tile numbers
        tileList := TStringList.Create;
        try
          ExtractStrings([','], [], PChar(t), tileList);
          i := 0;
          while i < tileList.Count do
          begin
            m.mask2[j,i] := StrToInt(tileList[i]);
            if m.mask2[j,i] > 0 then
            begin
              m.mask2Empty := false;
            end;
            Inc(i);
          end;
        finally
          tileList.Free;
        end;
        Inc(j);
        ReadLn(f, t);
      end;
    end;

    if AnsiPos('fringe"', t) > 0 then
    begin
      // Read in CSV layer data
      ReadLn(f, t);
      ReadLn(f, t);
      j := 0;
      while not(t = '</data>') do
      begin
        // Split string and populate array with tile numbers
        tileList := TStringList.Create;
        try
          ExtractStrings([','], [], PChar(t), tileList);
          i := 0;
          while i < tileList.Count do
          begin
            m.fringe[j,i] := StrToInt(tileList[i]);
            if m.fringe[j,i] > 0 then
            begin
              m.fringeEmpty := false;
            end;
            Inc(i);
          end;
        finally
          tileList.Free;
        end;
        Inc(j);
        ReadLn(f, t);
      end;
    end;

    if AnsiPos('fringe2', t) > 0 then
    begin
      // Read in CSV layer data
      ReadLn(f, t);
      ReadLn(f, t);
      j := 0;
      while not(t = '</data>') do
      begin
        // Split string and populate array with tile numbers
        tileList := TStringList.Create;
        try
          ExtractStrings([','], [], PChar(t), tileList);
          i := 0;
          while i < tileList.Count do
          begin
            m.fringe2[j,i] := StrToInt(tileList[i]);
            if m.fringe2[j,i] > 0 then
            begin
              m.fringe2Empty := false;
            end;
            Inc(i);
          end;
        finally
          tileList.Free;
        end;
        Inc(j);
        ReadLn(f, t);
      end;
    end;

    if AnsiPos('collision"', t) > 0 then
    begin
      // Read in CSV layer data
      ReadLn(f, t);
      ReadLn(f, t);
      j := 0;
      while not(t = '</data>') do
      begin
        // Split string and populate array with tile numbers
        tileList := TStringList.Create;
        try
          ExtractStrings([','], [], PChar(t), tileList);
          i := 0;
          while i < tileList.Count do
          begin
            m.collision[j,i] := StrToInt(tileList[i]);
            Inc(i);
          end;
        finally
          tileList.Free;
        end;
        Inc(j);
        ReadLn(f, t);
      end;
    end;

    if AnsiPos('<object ', t) > 0 then
    begin
      tmpStr := Copy(t, AnsiPos('gid', t)+5, Length(t));
      tmpStr := Copy(tmpStr, 1, AnsiPos('"', tmpStr)-1);
      objID := StrToInt(tmpStr);

      tmpStr := Copy(t, AnsiPos(' x=', t)+4, Length(t));
      tmpStr := Copy(tmpStr, 1, AnsiPos('"', tmpStr)-1);
      objX := StrToInt(tmpStr);

      tmpStr := Copy(t, AnsiPos(' y=', t)+4, Length(t));
      tmpStr := Copy(tmpStr, 1, AnsiPos('"', tmpStr)-1);
      objY := StrToInt(tmpStr);
      // I think Tiled has bottom left origin, and SDL has top left origin. So need to subtract a tile's height
      objY := objY - 16;

      case objID of
        PLAYER_START: begin
          playerX := objX;
          playerY := objY;
          camX := playerX - camW div 2 + 8;
          camY := playerY - camH div 2 + 8;
          cam^.x := Round(camX);
          cam^.y := Round(camY);
        end;
        WARP: begin
          tmpStr := Copy(t, AnsiPos('name', t)+6, Length(t));
          tmpStr := Copy(tmpStr, 1, AnsiPos('"', tmpStr)-1);
          tmpI := Length(m.objects);

          SetLength(m.objects, tmpI+1);
          m.objects[tmpI].x := objX;
          m.objects[tmpI].y := objY;
          m.objects[tmpI].objType := WARP;

          tileList := TStringList.Create;
          try
            ExtractStrings([','], [], PChar(tmpStr), tileList);
            m.objects[tmpI].data1 := StrToInt(tileList[0]);
            m.objects[tmpI].data2 := StrToInt(tileList[1])*16;
            m.objects[tmpI].data3 := StrToInt(tileList[2])*16;
          finally
            tileList.Free;
          end;
        end;
      end;
    end;
  end;
  CloseFile(f);
  Result := m;
end;

end.

