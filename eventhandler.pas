unit eventhandler;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, SDL2, renderer, gamestate;

procedure InitInput();
procedure HandleInput(var ev: PSDL_Event);

var
  f11Down: boolean;

implementation

procedure InitInput();
begin
  upDown := false;
  downDown := false;
  leftDown := false;
  rightDown := false;
  f11Down := false;
  upReleased := false;
  downReleased := false;
  zDown := false;
  xDown := false;
  cDown := false;
  spaceDown := false;
  returnDown := false;
end;

procedure HandleInput(var ev: PSDL_Event);
begin
  // Moved to game logic
  {if not(inputEnabled) then
    exit;}
  case ev^.type_ of
    SDL_KEYDOWN: begin
      case ev^.key.keysym.sym of
        SDLK_F11: begin
          if not(f11Down) then
          begin
            SetFullScreen(not GetFullScreen());
            f11Down := true;
          end;
        end;
        SDLK_UP: begin
          upDown := true;
          lastDown := DIR_UP;
        end;
        SDLK_DOWN: begin
          downDown := true;
          lastDown := DIR_DOWN;
        end;
        SDLK_LEFT: begin
          leftDown := true;
          lastDown := DIR_LEFT;
        end;
        SDLK_RIGHT: begin
          rightDown := true;
          lastDown := DIR_RIGHT;
        end;
        SDLK_z: begin
          zDown := true;
        end;
        SDLK_x: begin
          xDown := true;
        end;
        SDLK_c: begin
          cDown := true;
        end;
        SDLK_RETURN: begin
          returnDown := true;
        end;
        SDLK_SPACE: begin
          spaceDown := true;
        end;
      end;
    end;
    SDL_KEYUP: begin
      case ev^.key.keysym.sym of
        SDLK_F11: begin
          f11Down := false;
        end;
        SDLK_UP: begin
          upDown := false;
          upReleased := true;
          if lastDown = DIR_UP then
            lastDown := DIR_NONE;
        end;
        SDLK_DOWN: begin
          downDown := false;
          downReleased := true;
          if lastDown = DIR_DOWN then
            lastDown := DIR_NONE;
        end;
        SDLK_LEFT: begin
          leftDown := false;
          if lastDown = DIR_LEFT then
            lastDown := DIR_NONE;
        end;
        SDLK_RIGHT: begin
          rightDown := false;
          if lastDown = DIR_RIGHT then
            lastDown := DIR_NONE;
        end;
        SDLK_z: begin
          zDown := false;
        end;
        SDLK_x: begin
          xDown := false;
        end;
        SDLK_c: begin
          cDown := false;
        end;
        SDLK_RETURN: begin
          returnDown := false;
        end;
        SDLK_SPACE: begin
          spaceDown := false;
        end;
      end;
    end;
  end;
end;

end.

