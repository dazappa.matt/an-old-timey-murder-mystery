unit gamestate;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Math, SDL2;

procedure InitGameState();
procedure CleanUpGameState();

const
  DIR_NONE = -1;
  DIR_UP = 0;
  DIR_DOWN = 1;
  DIR_LEFT = 2;
  DIR_RIGHT = 3;

  OPT_PLAY = 0;
  OPT_QUIT = 1;

  SCREEN_LOAD = 0;
  SCREEN_MAIN_MENU = 1;
  SCREEN_OPTIONS = 2;
  SCREEN_GAME = 3;

var
  camW, camH: integer; // In pixels, not tile
  camWT, camHT, camHWT, camHHT: integer; // in tiles
  camX, camY, playerX, playerY, npcX, npcY: double;
  lastDown: Integer;
  upDown, downDown, leftDown, rightDown, upReleased, downReleased: boolean;
  zDown, xDown, cDown, spaceDown, returnDown: boolean;
  cam: PSDL_Rect;
  menuSelected, screen: Integer;
  gameRunning: boolean; // Game loop control variable. False will quit the game.
  blinkCounter, fadeAlpha: double;
  blinkVisible, blinkEnabled, inputEnabled, fadeIn, fadeOut: boolean;
  warpX, warpY: Integer;

implementation

procedure InitGameState();
begin
  playerX := 12*16;
  playerY := 8*16;
  camW := 320;
  camH := 180;
  camX := playerX - camW div 2 + 8;
  camY := playerY - camH div 2 + 8;
  camWT := Ceil(camW/16);
  camHT := Ceil(camH/16);
  npcX := 16;
  npcY := 16;
  lastDown := DIR_NONE;

  new(cam);
  cam^.x := Round(camX);
  cam^.y := Round(camY);
  cam^.w := 320;
  cam^.h := 180;

  menuSelected := OPT_PLAY;
  blinkCounter := 0;
  blinkVisible := true;
  blinkEnabled := true;
  inputEnabled := true;
  fadeAlpha := 0;
  fadeIn := true;
  fadeOut := false;

  warpX := -1;
  warpY := -1;
end;

procedure CleanUpGameState();
begin
  dispose(cam);
end;

end.

