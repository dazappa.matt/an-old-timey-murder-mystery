unit gameloop;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, SDL2, gamestate, map, assets;

procedure HandleWalkTile();
procedure GameLogic();
procedure InitGameLoop();

implementation

var
  timeNow, timeLast, movingDir: Integer;
  isMoving, downHandled, upHandled: boolean;
  distanceMoved, oldX, oldY: double;

// After movement is finished, this procedure is called to determine if action should be taken
// based on the tile underneath the player.
procedure HandleWalkTile();
var
  i, rx, ry: Integer;
begin
  i := 0;
  rx := Round(playerX);
  ry := Round(playerY);
  while i < Length(mTown.objects) do // TODO
  begin
    if (mTown.objects[i].x = rx) and (mTown.objects[i].y = ry) then
    begin
      case mTown.objects[i].objType of
        WARP: begin
          inputEnabled := false;
          fadeOut := true;
          warpX := mTown.objects[i].data2;
          warpY := mTown.objects[i].data3;
        end;
      end;
    end;
    Inc(i);
  end;
end;

procedure GameLogic();
var
  dt: double;
begin
  timeLast := timeNow;
  timeNow := SDL_GetPerformanceCounter();
  dt := (timeNow-timeLast)*1000 / SDL_GetPerformanceFrequency();

  case screen of
    SCREEN_MAIN_MENU:
    begin
      if blinkEnabled then
      begin
        blinkCounter := blinkCounter + dt;
        if (blinkCounter > 500) and (blinkCounter < 1000) then
          blinkVisible := false;
        if blinkCounter >= 1000 then
        begin
          blinkVisible := true;
          blinkCounter := 0;
        end;
      end else
      begin
        fadeAlpha := fadeAlpha + 0.5*dt;
        if fadeAlpha >= 255 then
        begin
          fadeAlpha := 255;
          screen := SCREEN_GAME;
        end;
      end;

      if downReleased then
        downHandled := false;
      if downDown and not(downHandled) then
      begin
        blinkVisible := true;
        blinkCounter := 0;
        downHandled := true;
        menuSelected := OPT_QUIT;
      end;

      if upReleased then
        upHandled := false;
      if upDown and not(upHandled) then
      begin
        blinkVisible := true;
        blinkCounter := 0;
        upHandled := true;
        menuSelected := OPT_PLAY;
      end;

      if zDown or xDown or cDown or spaceDown or returnDown then
      begin
        case menuSelected of
          OPT_PLAY:
          begin
            blinkVisible := true;
            blinkCounter := 0;
            blinkEnabled := false;
            inputEnabled := false;
          end;
          OPT_QUIT:
          begin
            gameRunning := false;
          end;
        end;
      end;
    end;
    SCREEN_GAME:
    begin
      if fadeIn then
      begin
        fadeAlpha := fadeAlpha - 0.5*dt;
        if fadeAlpha <= 0 then
        begin
          fadeAlpha := 0;
          fadeIn := false;
          inputEnabled := true;
        end;
      end;

      if fadeOut then
      begin
        fadeAlpha := fadeAlpha + 0.5*dt;
        if fadeAlpha >= 255 then
        begin
          fadeAlpha := 255;
          // TODO: set map here
          playerX := warpX;
          playerY := warpY;
          camX := playerX - camW div 2 + 8;
          camY := playerY - camH div 2 + 8;
          cam^.x := Round(camX);
          cam^.y := Round(camY);
          fadeIn := true;
          fadeOut := false;
        end;
      end;

      if not(isMoving) then
      begin
        if not(inputEnabled) then
          exit;
        if lastDown > DIR_NONE then
        begin
          case lastDown of
            DIR_UP:
            begin
              // TODO: make current map
              if mTown.collision[Round(playerY/16)-1,Round(playerX/16)] = BLOCKED then
                exit;
              movingDir := DIR_UP;
              oldY := playerY;
              isMoving := true;
            end;
            DIR_DOWN:
            begin
              if mTown.collision[Round(playerY/16)+1,Round(playerX/16)] = BLOCKED then
                exit;
              movingDir := DIR_DOWN;
              oldY := playerY;
              isMoving := true;
            end;
            DIR_LEFT:
            begin
              if mTown.collision[Round(playerY/16),Round(playerX/16)-1] = BLOCKED then
                exit;
              movingDir := DIR_LEFT;
              oldX := playerX;
              isMoving := true;
            end;
            DIR_RIGHT:
            begin
              if mTown.collision[Round(playerY/16),Round(playerX/16)+1] = BLOCKED then
                exit;
              movingDir := DIR_RIGHT;
              oldX := playerX;
              isMoving := true;
            end;
          end;
        end else
        begin
          if upDown then
          begin
            if mTown.collision[Round(playerY/16)-1,Round(playerX/16)] = BLOCKED then
              exit;
            movingDir := DIR_UP;
            oldY := playerY;
            isMoving := true;
          end
          else if downDown then
          begin
            if mTown.collision[Round(playerY/16)+1,Round(playerX/16)] = BLOCKED then
              exit;
            movingDir := DIR_DOWN;
            oldY := playerY;
            isMoving := true;
          end
          else if leftDown then
          begin
            if mTown.collision[Round(playerY/16),Round(playerX/16)-1] = BLOCKED then
              exit;
            movingDir := DIR_LEFT;
            oldX := playerX;
            isMoving := true;
          end
          else if rightDown then
          begin
            if mTown.collision[Round(playerY/16),Round(playerX/16)+1] = BLOCKED then
              exit;
            movingDir := DIR_RIGHT;
            oldX := playerX;
            isMoving := true;
          end;
        end;
      end;

      case movingDir of
        DIR_UP:
        begin
          if distanceMoved >= 16.0 then
          begin
            distanceMoved := 0;
            movingDir := DIR_NONE;
            playerY := oldY - 16;
            isMoving := false;
            HandleWalkTile();
          end else
          begin
            playerY := playerY - 0.025*dt;
            distanceMoved := distanceMoved + 0.025*dt;
          end;
        end;
        DIR_DOWN:
        begin
          if distanceMoved >= 16.0 then
          begin
            distanceMoved := 0;
            movingDir := DIR_NONE;
            playerY := oldY + 16;
            isMoving := false;
            HandleWalkTile();
          end else
          begin
            playerY := playerY + 0.025*dt;
            distanceMoved := distanceMoved + 0.025*dt;
          end;
        end;
        DIR_LEFT:
        begin
          if distanceMoved >= 16.0 then
          begin
            distanceMoved := 0;
            movingDir := DIR_NONE;
            playerX := oldX - 16;
            isMoving := false;
            HandleWalkTile();
          end else
          begin
            playerX := playerX - 0.025*dt;
            distanceMoved := distanceMoved + 0.025*dt;
          end;
        end;
        DIR_RIGHT:
        begin
          if distanceMoved >= 16.0 then
          begin
            distanceMoved := 0;
            movingDir := DIR_NONE;
            playerX := oldX + 16;
            isMoving := false;
            HandleWalkTile();
          end else
          begin
            playerX := playerX + 0.025*dt;
            distanceMoved := distanceMoved + 0.025*dt;
          end;
        end;
      end;

      camX := playerX - camW div 2 + 8;
      camY := playerY - camH div 2 + 8;
      cam^.x := Round(camX);
      cam^.y := Round(camY);
    end;
  end;
end;

procedure InitGameLoop();
begin
  timeNow := SDL_GetPerformanceCounter();
  timeLast := 0;
  isMoving := false;
  movingDir := DIR_NONE;
  distanceMoved := 0;
  oldX := 0;
  oldY := 0;
  downHandled := false;
  upHandled := false;
end;

end.

